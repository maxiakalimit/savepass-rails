module StoragesHelper
  def strip_url(url)
    url.sub(/https\:\/\//, '').sub(/.com/, '')     if url.include? "https://"
    url.sub(/https\:\/\/www./, '').sub(/.com/, '') if url.include? "https://www."
    url.sub(/http\:\/\/www./, '').sub(/.com/, '')  if url.include? "http://www."
    url.sub(/www./, '').sub(/.com/, '')            if url.include? "www."
    return url
  end
end
