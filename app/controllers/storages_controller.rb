class StoragesController < ApplicationController
  def index
    if current_user
      @storages = current_user.storages.all

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @storages }
      end
    else
      #render json: "Вы должны авторизироваться на сайте"
      render json: { error:true, message: "Вы должны авторизироваться на сайте http://savepass.xxivcreate.xyz/" }
    end

  end

  def show
    @storage = Storage.find(params[:id])
  end

  def new
    #@storage = Storage.new
    @storage = current_user.storages.build
    #super
  end

  def edit
   @storage = Storage.find(params[:id])
  end

  def create
    @storage = current_user.storages.build(storage_params)
    @public_key = current_user.public_key
    @private_key             = File.open("#{Dir.pwd}/private_key/private_key-#{current_user.id}.pem", 'r'){ |file| file.read }
    @storage.login           = Cryptosyst.encrypt( @storage.login, @public_key.to_i )
    @storage.password        = Cryptosyst.encrypt( @storage.password, @public_key.to_i )
    @storage.decoderlogin    = Cryptosyst.decrypt( @storage.login.to_i, @public_key.to_i, @private_key.to_i )
    @storage.decoderpassword = Cryptosyst.decrypt( @storage.password.to_i, @public_key.to_i, @private_key.to_i )
    if @storage.save
      redirect_to root_url
    else
      redirect_to root_url
    end

  end

  def destroy
     @storage  = Storage.find(params[:id])
     @storage.destroy

     render json: { success:true }
  end

  private
  def storage_params
    params.require(:storage).permit(:url, :name, :login, :password, :note)
  end
end
