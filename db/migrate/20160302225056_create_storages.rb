class CreateStorages < ActiveRecord::Migration
  def change
    create_table :storages do |t|
      t.string :user_id

      t.string :url
      t.string :name
      t.string :login
      t.string :password
      t.text   :decoderlogin
      t.text   :decoderpassword
      t.text   :note
      t.string :logo
      t.text   :favourite
      t.timestamps null: false
    end
  end
end
